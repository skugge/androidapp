package com.example.myapplication.classes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.myapplication.AllBusesActivity;
import com.example.myapplication.AllChannelsActivity;
import com.example.myapplication.LoginActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.ScanActivity;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;
;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * The connect and Message Handler for the webSocket connection.
 * It's main job is to connect to the server and manage incoming and outgoing messages.
 * Created by tjabe on 02.02.2018.
 *
 * This class is strongly influenced by following source:
 * @see <a href="https://github.com/socketio/socket.io-client-java/issues/26"></a>
 */

public class ConnectionHandler {
    private WebSocketClient socket;
    private String name;
    private String pwd;
    private boolean https;
    private String host = "18.188.200.130:3000"; //10.22.191.133
    private int volumePercentage;
    public JSONObject channelInfo;
    private JSONObject playbackStateJson = new JSONObject();
    public static final String TAG = "ConnectionHandler";
    public String channelGroup = "12C";
    public String standardChannel = "NRK P13";
    public String[] tmp ={"NRK P1", "BBC", "P3","Radio Norge"};
    private Context mainCtx;
    private Context loginCtx;
    private Context menuCtx;
    private Context scanCtx;
    private Context busesCtx;
    private Context channelsCtx;
    private Context startCtx;
    private SharedPreferences sharedpreferences;

    /**
     * This is the constructor.
     * On run it creates the socket object, and takes following variables:
     * @param name      The username
     * @param pwd       The Password
     * @param context   context of the activity that created the connection handler.
     */
    public ConnectionHandler(String name, String pwd,Context context){
        this.name = name;
        this.pwd = pwd;
        this.startCtx = context;
        this.socket = getSocket(name,pwd);
    }

    /**
     * Set volume method,
     * @param step  Boolean for direction. True --> UP, False --> DOWN.
     */
    public void volume(boolean step){
        if(step){
            socket.send("volume:up");
        }else{
            socket.send("volume:down");
        }
    }

    /**
     * Set mute method
     * Sends a mute message to the server, and thereby to the connected client in the same room.
     */
    public void mute(){
        socket.send("volume:mute");
    }



    //Context setters:
    /**
     * Sets the mainActivity context
     * @param ctx
     */
    public void setLoginCtx(Context ctx){ loginCtx = ctx;}

    /**
     * Sets the mainActivity context
     * @param ctx
     */
    public void setMainCtx(Context ctx){ mainCtx = ctx;}

    /**
     * Sets the menuActivity context
     * @param ctx
     */
    public void setMenuCtx(Context ctx){ menuCtx = ctx;}

    /**
     * Sets the AllBusesActivity context
     * @param ctx
     */
    public void setBusesCtx(Context ctx){
        try{
            ((Activity) mainCtx).finish();
        }catch (Exception e){
            Log.e(TAG,"Already dead!"+e);
        }
        busesCtx = ctx;
    }

    /**
     * Sets the AllChannelsActivity context.
     * @param ctx
     */
    public void setChannelsCtx(Context ctx){ channelsCtx = ctx; }



    //Message methods
    public void createRoom(int id){
        socket.send("createRoom:"+id);
    }

    public void logout(){
        try{
            ((Activity) channelsCtx).finish();
        }catch (Exception e){
            Log.e(TAG,"Most likely not init'ed");
        }
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if(sharedpreferences.contains("token")){
            editor.remove("token");
            editor.apply();
        }
    }

    public void playPause(boolean  playbackState, String channel){
        try {
            playbackStateJson = new JSONObject();
            playbackStateJson.put("playbackState",playbackState);
            playbackStateJson.put("channel",channel);
            playbackStateJson.put("channelGroup",channelGroup);
            playbackStateJson.put("standardChannel",standardChannel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Read",""+ playbackStateJson);
        socket.send("playStation:"+channel);
    }

    public void scan(Context ctx){
        Intent intent = new Intent(ctx,ScanActivity.class);
        intent.putExtra("Scan", "full");
        ctx.startActivity(intent);
        ((Activity) ctx).finish();
    }

    public void getStations(Context ctx){
        scanCtx = ctx;
        socket.send("getStations");
    }

    public void getScan(Context ctx){
        scanCtx = ctx;
        socket.send("scan");
    }

    /**
     * The socket create method.
     * This method request the https webSocket connection, and tries to login with given parameters:
     * @param name  The UsernameClosed
     * @param pwd   The password
     * @return      The socket object on connection.
     */
    public synchronized WebSocketClient getSocket(final String name, final String pwd) {
        URI uri;
        String websocketEndPointUrl = "";
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(startCtx);
        WebSocketClient mWebSocketClient = null;
        try {


            websocketEndPointUrl="wss://"+host;
            Log.i(TAG, " WSURL: " + websocketEndPointUrl);

            uri = new URI(websocketEndPointUrl);

            try {
                mWebSocketClient = new WebSocketClient(uri,new Draft_17())
                {
                    @Override
                    public void onOpen(ServerHandshake serverHandshake) {
                        Log.i("Websocket", "Opened");
                        this.send("imaUser:"+ name +":"+pwd);
                    }

                    @Override
                    public void onMessage(String s) {
                        String data[] = s.split(":");
                        String identifier = data[0];
                        Log.e(TAG,"Message: "+identifier);
                        Log.e(TAG,"Length: "+data.length);
                        switch (identifier){
                            case "authenticated" :
                                if (data.length > 1){
                                    if (data[1].equals("true")){
                                        //Authenticated, save Token
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        if(data.length == 3){
                                            String val = name + ":" + data[2];
                                            editor.putString("token",val);
                                            editor.apply();
                                        }
                                        startCtx.startActivity(new Intent(startCtx,AllBusesActivity.class));
                                        ((Activity) startCtx).finish();
                                    }else{
                                        Log.e(TAG,"Not Authorised!!");
                                        //not authorised, remove token
                                        logout();

                                        if(loginCtx != null ){
                                            ((LoginActivity) loginCtx).emptyInput();
                                        }else{
                                            startCtx.startActivity(new Intent(startCtx,LoginActivity.class));
                                            ((Activity) startCtx).finish();
                                            loginCtx = null;
                                        }
                                        //and start login
                                    }
                                }else{
                                    Log.e(TAG,"Wrong syntax!");
                                    Log.e(TAG,"Not Authorised!!");
                                }

                                break;
                            case "room" :
                                if(data[1].equals("true")){
                                    Log.e("Added to room",data[1]);
                                    Intent intent = new Intent(busesCtx,ScanActivity.class);
				                    intent.putExtra("Scan","getStations");
				                    busesCtx.startActivity(intent);
                                    ((Activity) busesCtx).finish();
                                    busesCtx = null;
                                }else{
                                    Log.e("Removed from room!",data[1]);
                                    //Clearing the channels array.
                                    MainActivity.channels = new String[0];
                                    if(channelsCtx != null){
                                        channelsCtx.startActivity(new Intent(channelsCtx,AllBusesActivity.class));
                                        ((Activity) channelsCtx).finish();
                                        channelsCtx = null;
                                    }else{
                                        Log.e(TAG,"channelCtx did no exist!");
                                    }
                                    if(menuCtx != null){
                                        menuCtx.startActivity(new Intent(menuCtx,AllBusesActivity.class));
                                        ((Activity) menuCtx).finish();
                                        menuCtx = null;
                                    }else{
                                        Log.e(TAG,"menuCtx did no exist!");
                                    }
                                }
                                break;
                            case "buses":
                                if(data.length < 2){
                                    Log.e(TAG,"No Busses available!");
                                    MainActivity.buses = new String[0];
                                    ((AllBusesActivity) busesCtx).emptyList();
                                }else{
                                    Log.e("Busses received",data[1]);
                                    MainActivity.buses = data[1].split(",");
                                    if (busesCtx != null){
                                        ((AllBusesActivity) busesCtx).refreshList();
                                    }
                                }
                                break;
                            case "stations" :
                                if(data.length < 2){
                                    Log.e(TAG,"No channels available!");
                                    MainActivity.channels = new String[0];
                                    //Log.e(TAG,""+MainActivity.channels[0]);
                                    scanCtx.startActivity(new Intent(scanCtx,AllChannelsActivity.class));
                                }else{
                                    Log.e(TAG,"Channels received");
                                    MainActivity.channels = data[1].split("\n");
                                    scanCtx.startActivity(new Intent(scanCtx,AllChannelsActivity.class));
                                }
                                ((Activity) scanCtx).finish();
                                break;
                            default:
                                Log.e("Tag","Given syntax not allowed !!!"+data.toString());
                        }
                    }

                    @Override
                    public void onClose(int i, String s, boolean b) {
                        Log.i("Websocket", "Closed " + s);
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.i("Websocket", "Error " + e.getMessage());
                    }
                };

                if (websocketEndPointUrl.indexOf("wss") == 0)
                {
                    try {
                        SSLContext sslContext = null;
                        sslContext = SSLContext.getInstance( "TLS" );
                        sslContext.init( null, trustAllCerts, null );
                        // sslContext.init( null, null, null ); // will use java's default key and trust store which is sufficient unless you deal with self-signed certificates
                        SSLSocketFactory factory = sslContext.getSocketFactory();// (SSLSocketFactory) SSLSocketFactory.getDefault();

                        mWebSocketClient.setSocket(factory.createSocket());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
                mWebSocketClient.connect();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        } catch (URISyntaxException e) {
            Log.e(TAG, e.getMessage());
        }

        return mWebSocketClient;
    }


    //Helper Methods
    private TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[]{};
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
    }};
}
