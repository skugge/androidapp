package com.example.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import static com.example.myapplication.MainActivity.ch;

public class ScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        String s = getIntent().getStringExtra("Scan");
        String TAG = "ScanActivity";
        if(s.equals("full")){
            Log.e(TAG,"sending: "+s);
            ch.getScan(ScanActivity.this);
        }else if(s.equals("getStations")){
		    Log.e(TAG,"sending: "+s);
		    ch.getStations(ScanActivity.this);
        }
    }
}
