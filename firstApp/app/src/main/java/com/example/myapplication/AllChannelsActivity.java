package com.example.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioTrack;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;


import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static com.example.myapplication.MainActivity.ch;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;


/**
 * https://github.com/umano/AndroidSlidingUpPanel/blob/master/demo/src/main/java/com/sothree/slidinguppanel/demo/DemoActivity.java
 */
public class AllChannelsActivity extends ListActivity {
    public final static String MESSAGE_KEY ="data.channel";
    public final static String TAG = "AllChannelsActivity";
    private boolean favoriteBtn = false;
    private boolean stopBtn = false;
    static ArrayAdapter<String> adapter;
    private SlidingUpPanelLayout mLayout;
    private Runnable run;
    private boolean first  = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allchannels);
        ch.setChannelsCtx(AllChannelsActivity.this);
        Log.e(TAG,""+MainActivity.channels.length);
        adapter = new ArrayAdapter<String>(getListView().getContext(), android.R.layout.simple_list_item_1, new ArrayList<>(Arrays.asList(MainActivity.channels)));
        Log.e(TAG,adapter.toString());
        ListView lv = getListView();
        lv.setEmptyView(findViewById(R.id.empty));
        lv.setAdapter(adapter);
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.setEnabled(false);
        ImageButton scanBtn = (ImageButton) findViewById(R.id.scanBtn);

        run = new Runnable() {
            public void run(){
                adapter.clear();
                adapter.addAll(MainActivity.channels);
                adapter.notifyDataSetChanged();
            }
        };


        // Listeners:
        lv.setOnItemClickListener((parent, view, position, id) -> {
            if (first){
                first = false;
                mLayout.setEnabled(true);
                //sett the buttons to visible
                ImageButton firstButton = (ImageButton) findViewById(R.id.imageButton);
                TextView textView3 = (TextView) findViewById(R.id.textView13);
                firstButton.setVisibility(View.VISIBLE);
                textView3.setVisibility(View.VISIBLE);
            }
            String channel = MainActivity.channels[position];
            ch.playPause(true,channel);
            TextView textView2 = (TextView) findViewById(R.id.textView12);
            TextView textView3 = (TextView) findViewById(R.id.textView13);
            textView2.setText(channel);
            textView3.setText(channel);
            mLayout.setPanelState(PanelState.EXPANDED);
        });

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //v.startAnimation(myAnim);
                ch.scan(AllChannelsActivity.this);
            }
        });

        mLayout.addPanelSlideListener(new PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onSlide");
            }

            @Override
            public void onPanelStateChanged(View panel, PanelState previousState, PanelState newState) {
                Log.i(TAG, "onPanelStateChanged ");
            }
        });
    }

    public void openMenu(View v){ListView lv = getListView();
        startActivity(new Intent(AllChannelsActivity.this, MenuActivity.class));
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.d("Volume","  Up!");
                    ch.volume(true);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.d("Volume","  Down!");
                    ch.volume(false);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public void closeOverlay(View v){
        mLayout.setPanelState(PanelState.COLLAPSED);
    }

    public void stopPlay(View v){
        ImageButton secondButton = (ImageButton) findViewById(R.id.imageButton4);
        ImageButton firstButton = (ImageButton) findViewById(R.id.imageButton);

        int icon;

        if (stopBtn) {
            stopBtn = false;
            icon = R.drawable.stop;
            ch.mute();
        }

        else {
            stopBtn = true;
            icon = R.drawable.play;
            ch.mute();
        }
        firstButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), icon));
        secondButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), icon));
    }

}
