package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.myapplication.classes.ConnectionHandler;


public class MainActivity extends Activity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    public static final String TAG = "MainActivity";
    public static ConnectionHandler ch;
    public static String[] channels = {};
    public static String[] buses = {};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //If a token exists, retrieve the username and token
        //And try to establish a connection
        //Authentication is handled by ConnectionHandler
        if(sharedpreferences.contains("token")){
            String val = sharedpreferences.getString("token","DEFAULT");
            Log.e(TAG,val);
            String name = val.substring(0,val.indexOf(':'));
            String token = val.substring(val.indexOf(':')+1,val.length());
            Log.d(TAG,"Name: "+name);
            Log.d(TAG,"Token: "+token);
            ch = new ConnectionHandler(name,token,MainActivity.this.getApplicationContext());
            ch.setMainCtx(MainActivity.this);
        }else{
            //If user doesn't have a token redirect to login
            //User will receive a new token on a successful login
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }
}