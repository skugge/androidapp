
package com.example.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myapplication.classes.ConnectionHandler;

import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import static com.example.myapplication.MainActivity.ch;

public class LoginActivity extends Activity {

    public String user;
    public String TAG = "LoginActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void login(View v)throws NoSuchAlgorithmException{
        EditText nameBox = (EditText) findViewById(R.id.username);
        EditText pwdBox = (EditText) findViewById(R.id.password);
        String name = nameBox.getText().toString();
        String pwd = pwdBox.getText().toString();

        /*
        Example code taken from https://stackoverflow.com/questions/9126567/method-not-found-using-digestutils-in-android
         */
        String hash = new String(Hex.encodeHex(DigestUtils.sha256(pwd)));

        Log.e(TAG,"Tried logging in using Name : "+name+" and pwd: "+hash);
        try{
            MainActivity.ch = new ConnectionHandler(name,hash,LoginActivity.this);
            ch.setLoginCtx(LoginActivity.this);
            overridePendingTransition(R.anim.hold, R.anim.fadeinright);
        }catch (Exception e){
            Log.e(TAG,"Your Login attempt was rejected!");
        }
    }

    public void emptyInput(){
        EditText nameBox = (EditText) findViewById(R.id.username);
        EditText pwdBox = (EditText) findViewById(R.id.password);
        nameBox.setText("", TextView.BufferType.EDITABLE);
        pwdBox.setText("", TextView.BufferType.EDITABLE);
    };
}
