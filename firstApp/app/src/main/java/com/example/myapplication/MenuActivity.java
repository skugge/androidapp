package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


import static com.example.myapplication.MainActivity.ch;


public class MenuActivity extends Activity {
    public Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ch.setMenuCtx(this);
        setContentView(R.layout.activity_menu);
        addListenerOnButton();
    }
    public void closeMenu(View v){
        finish();
    }


    public void addListenerOnButton() {

        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //loggut
                ch.logout();
                startActivity(new Intent(MenuActivity.this, LoginActivity.class));
                finish();
            }

        });
    }
}

