package com.example.myapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import static com.example.myapplication.MainActivity.ch;

public class AllBusesActivity extends ListActivity {

    public final static String MESSAGE_KEY ="data.channel";
    public final static String TAG = "AllBusesActivity";
    static ArrayAdapter<String> adapter;
    private ListView lv;
    private Runnable run;
    private Runnable runE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_buses);
        ch.setBusesCtx(AllBusesActivity.this);
        adapter = new ArrayAdapter<>(getListView().getContext(), android.R.layout.simple_list_item_1, new ArrayList<>(Arrays.asList(MainActivity.buses)));
        lv = getListView();
        lv.setEmptyView(findViewById(R.id.empty));
        lv.setAdapter(adapter);
        run = new Runnable() {
            public void run(){
                adapter.clear();
                adapter.addAll(MainActivity.buses);
                adapter.notifyDataSetChanged();
            }
        };
        runE = new Runnable() {
            public void run(){
                adapter.clear();
                adapter.addAll(new ArrayList<>());
                adapter.notifyDataSetChanged();
            }
        };

        //Lytter:
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String bus = MainActivity.buses[position];
                //ch.playPause(true,channel);
                ch.createRoom(Integer.parseInt(bus));
                Log.e(TAG,"send create room");
            }
        });
    }
    public void refreshList(){
        runOnUiThread(run);
        Log.e(TAG,"Refreshed!");
    }

    public void emptyList(){
        runOnUiThread(runE);
        Log.e(TAG,"Emptied!");
    }

    public void openMenu(View v){ListView lv = getListView();
        startActivity(new Intent(AllBusesActivity.this, MenuActivity.class));
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.d("Volume","  Up!");
                    ch.volume(true);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.d("Volume","  Down!");
                    ch.volume(false);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    private static String[] getAndConvertJson(){

        String[] temp = {"Bus 1","Bus 3","Bus 4","Bus 6","Bus 7","Bus 14","Bus 34"};
        return temp;
    }
}
